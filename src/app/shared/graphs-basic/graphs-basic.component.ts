import { Component, OnInit } from '@angular/core';
import {colors} from '../const/colors';

@Component({
  selector: 'app-graphs-basic',
  templateUrl: './graphs-basic.component.html',
  styleUrls: ['./graphs-basic.component.scss']
})
export class GraphsBasicComponent implements OnInit {

  // Start Configuration
  title = 'PRODUCCIóN DE ENERGíA RENOVABLE';
  subtitle = 'Producción eléctrica renovable en gigavatio-hora (GWh)';
  cantToShow = 16;
  currentYear = 0;
  timeSleep = 500;
  csv = 'assets/csv/Energía_Sostenible_para_Todos.csv';
  csv1 = 'assets/csv/Energía_Sostenible_para_Todos_Latinoamerica.csv';
  // csv = 'assets/csv/ICP.csv';
  // End Configuration

  datas = [];
  datas1 = [];
  years = [];
  temp = new Set();
  currentFlag = '';
  init = 3;
  vel = 500;
  imgSelected = '';

  constructor() { }

  readTextFile(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          t.forEach((line, idx) => {

            const data = line.split(',');

            if (idx === 0) {
              this.years = data.slice(this.init);
            } else if (idx !== 0 && line) {
              const name = data[0];
              let values = data.slice(this.init).map((element) => { 
                if ( isNaN(parseFloat(element.replace(/\s/g, ''))) ) {
                  return 0;
                }
                return parseFloat(element.replace(/\s/g, '')); 
              });
              values.unshift(0);
              const countryFlag = data[2];
              let id = '';
              name.split(' ').forEach((elem, idx) => {
                if (idx != 0)
                  id += '-';
                id += elem;
              });
              const logo = data[2];
              const salario = data[3];
              this.datas.push(
                {
                  idx: idx,
                  name,
                  values,
                  position: this.cantToShow,
                  width: 0,
                  value: 0,
                  countryFlag,
                  id: id,
                  color: colors[data[1]],
                  flag: data[1],
                  logo,
                  year: data[1],
                  salario
                }
              );
            }
          });
        }
      }
    };
    rawFile.send(null);
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  floorValue(val) {
    return Math.round(val);
  }

  decimalValue(val) {
    return (val).toFixed(2);
  }

  miles(cant) {
    cant = cant.trim();
    let solve = '';
    let cont = 1;
    for (let i = cant.length - 1; i >= 0; i--) {
      solve = cant[i] + solve;
      if (cont % 3 === 0 && i > 0) {
        solve = '.' + solve;
      }
      cont++;
    }
    return solve;
  }

  calculatePosition() {
    this.datas.forEach((data, idx) => {
      let solve = 0;      
      if (data.width === 0) {
        data.position = this.cantToShow;
      } else {
        this.datas.forEach((data1, idx1) => {
          if (idx != idx1) {
            if (data.width < data1.width)
              solve++;
            else if (data.width === data1.width && data.idx < data1.idx)
              solve++;
          }
        });
        if ( solve >= 15 ) solve = this.cantToShow
        data.position = solve;
      }
      if (data.position <= 14) {
        this.temp.add( data.name + "_" + data.idx)
      }
    });
  }

  calculate(percent, position) {
    let max = 0;
    this.datas.forEach(data => {
      const diff = data.values[position + 1] - data.values[position];
      const ant = data.values[position];
      data.value = (ant + (percent * diff) / this.vel);
      max = Math.max(max, data.value);
      if (diff >= 1)
        this.imgSelected = data.flag;
    });
    this.datas.forEach(data => {
      data.width = isNaN( 55 * (data.value / max)) ? 0 : 55 * (data.value / max);
    });
    this.calculatePosition();
  }

  async execute() {
    console.log(this.datas);
    for (let i = 0; i < this.years.length; i++) {
      // await this.sleep(1000);
      this.currentYear = this.years[i];
      for (let j = 1; j <= this.vel; j++) {
        await this.sleep(1);
        this.calculate(j, i);
      }
    }
  }

  compute() {
    this.readTextFile(this.csv);
    this.execute();
  }

  async ngOnInit() {
    this.compute();
  }

  generarNumero(numero) {
    return (Math.random() * numero).toFixed(0);
  }

  colorRGB() {
    var coolor = "(" + this.generarNumero(255) + "," + this.generarNumero(255) + "," + this.generarNumero(255) + ")";
    return "rgb" + coolor;
  }

}
