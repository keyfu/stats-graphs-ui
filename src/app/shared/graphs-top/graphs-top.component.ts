import { Component, Input, OnInit } from '@angular/core';
import { colors } from '../const/colors';

@Component({
  selector: 'app-graphs-top',
  templateUrl: './graphs-top.component.html',
  styleUrls: ['./graphs-top.component.scss']
})
export class GraphsTopComponent implements OnInit {

  colors = colors;

  // Start Configuration
  title = 'TOP PAÍSES CON MAYOR HUELLA ECOLÓGICA PER CÁPITA';
  subtitle = 'Huella ecológica por persona - Biocapacidad por persona (gha -> hectáreas globales)';
  @Input() csv = 'assets/csv/huella-ecologica.csv';
  datas = [];
  datasToShow = [];
  paso = 0.10;
  cantToShow = 17;
  cont = 0;
  contCopy = 0;
  current = 0;
  top = 0;
  actual = 0;
  currentCopy = 0;
  // End Configuration

  max = 0;
  imageSelected = '';
  citySelected = '';

  imageSelected1 = '';
  citySelected1 = '';

  constructor() { }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  truncate(text, cant) {
    return cant > text.length ? text.slice(0, cant) : text.slice(0, cant) + '...';
  }

  async compute() {
    this.readTextFile(this.csv);

    // ordenar de menor a mayor las cantidades
    this.datas.sort((a, b) => {
      if ( parseFloat(a.cant) < parseFloat(b.cant) ) return - 1;
      if ( parseFloat(a.cant) > parseFloat(b.cant) ) return 1;
      if ( parseFloat(a.cant) == parseFloat(b.cant) ) {
        if ( a.number > b.number ) 
          return -1;
        return 1;
      }
      return 0;
    });

    this.top = this.datas.length + 1;

    // start precalc
    for (let j = 0; j < this.datas.length; j++) {
      this.max = this.datas[j].cant;

      for (let i = 0; i < this.cont; i++) {
        const width = ((this.datas[i].cant / this.max) * 70);
        const diff = this.datas[i].widthInt - width;
        let t = j * 700;
        for (let k = 700; k > 0; k--) {
          const widthActual = (width + ( ((k/700)) * diff ) );
          const tmp = (80*widthActual)/100;
          this.datas[i].widthActuals[t] = tmp;
          this.datas[i].currentValues[t] = this.datas[i].cant;
          this.datas[i].countryPositions[t] = ( tmp + 20 );
          this.datas[i].cantPositions[t] = ( tmp + 22 );
          t++;
          this.datas[i].widthInt = width;
          this.datas[i].width = width.toString() + '%';
        }
      }
      this.currentCopy = j * 700;
      for (let i = 1; i <= 70; i += 0.10) {
        this.datas[j].width = i.toString() + '%';
        this.currentCopy++;
        const tmp = (80*i)/100;
        this.datas[j].widthActuals[this.currentCopy] = tmp;
        this.datas[j].currentValues[this.currentCopy] = this.datas[j].cant;
        this.datas[j].widthInt = i;
        this.datas[j].countryPositions[this.currentCopy] = (tmp + 20);
        this.datas[j].cantPositions[this.currentCopy] = (tmp + 22);
      }
      this.cont++;
    }

    // end precalc

    this.cont = 0;

    await this.sleep(1000);

    for (let j = 0; j < this.datas.length; j++) {
      // await this.sleep(1000);
      this.top --;
      this.max = this.datas[j].cant;

      if ( j%2 !== 0 ) {
        this.imageSelected1 = this.datas[j].imgCity;
        this.citySelected1 = this.datas[j].name;
      }
      else {
        this.imageSelected = this.datas[j].imgCity;
        this.citySelected = this.datas[j].name;  
      }
      

      for (let i = 0; i < this.cont; i++) {
          this.datas[i].position ++;
      }

      this.datas[j].position = 0;
      this.current = j * 700;
      for (let i = 1; i <= 70; i += 0.10) {
          await this.sleep(1).then( () => {
            this.current++;
          }
        );
      }
      this.cont++;
    }

  }

  getValue(top) {
    if ( top < 10 ) {
      return '0' + top.toString();
    }
    return top;
  }

  miles(cant) {
    cant = cant;
    let solve = '';
    let cont = 1;
    for ( let i=cant.length - 1; i>=0; i -- ) {
      solve = cant[i] + solve;
      if ( cont % 3 === 0 && i > 0 ) {
        solve = '.' + solve;
      }
      cont ++;
    }
    return solve;
  }

  async ngOnInit() {
    this.compute();
  }

  readTextFile(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          let number = 1;
          t.forEach((line, idx) => {
            if (idx === 0) {
              const data = line.split(',');
            }
            else if (line) {
              const data = line.split(',');
              const name = data[0].trim();
              const color = data[1];
              const country = data[4].split('|')[0];
              let country1, country2;
              if ( data[4].split('|').length >= 2 ) {
                country1 = data[4].split('|')[1];
              }
              if ( data[4].split('|').length >= 3 ) {
                country2 = data[4].split('|')[2];
              }
              const cant = parseFloat(data[3]);
              const nameShort = data[2].trim();
              this.datas.push({
                number,
                name,
                color,
                country,
                country1,
                country2,
                imgCity: 'assets/cities/' + data[5] + '.jpg',
                cant,
                width: '0%',
                position: this.cantToShow,
                widthActuals: [],
                positionActuals: [],
                currentValues: [],
                cantPositions: [],
                countryPositions: [],
                nameShort,
                img: country,
                pib: data[6]
              });
              number++;
            }
          });
        }
      }
    };
    rawFile.send(null);
  }

  generarLetra(){
    var letras = ["a","b","c","d","e","f","0","1","2","3","4","5","6","7","8","9"];
    var numero = (Math.random()*15).toFixed(0);
    return letras[numero];
  }
    
  colorHEX(){
    var coolor = "";
    for(var i=0;i<6;i++){
      coolor = coolor + this.generarLetra() ;
    }
    return "#" + coolor;
  }

}
