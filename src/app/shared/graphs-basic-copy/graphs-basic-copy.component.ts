import { Component, OnInit, Input } from '@angular/core';
import {colors} from '../const/colors';

@Component({
  selector: 'app-graphs-basic-copy',
  templateUrl: './graphs-basic-copy.component.html',
  styleUrls: ['./graphs-basic-copy.component.scss']
})
export class GraphsBasicCopyComponent implements OnInit {

  @Input() color = 'rgb(83, 36, 132)';

  // Start Configuration
  @Input() height = '277px';
  @Input() left = '1230px';
  @Input() top = '-820px';
  @Input() title = 'Casos Confirmados';
  // subtitle = 'Cantidad de turistas que arriban al país';
  cantToShow = 15;
  currentYear = 0;
  timeSleep = 1000;
  @Input() csv = 'assets/csv/myCOVID1.csv';
  // End Configuration

  datas = [];
  years = [];
  @Input() vel = 500;
  init = 3;

  temp = new Set();

  constructor() { }

  readTextFile(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          t.forEach((line, idx) => {

            const data = line.split(',');

            if (idx === 0) {
              this.years = data.slice(this.init);
            } else if (idx !== 0 && line) {
              const name = data[0];
              let values = data.slice(this.init).map((element) => { 
                if ( isNaN(parseFloat(element.replace(/\s/g, ''))) ) {
                  return 0;
                }
                return parseFloat(element.replace(/\s/g, '')); 
              });
              values.unshift(0);
              const countryFlag = data[2];
              let id = '';
              name.split(' ').forEach((elem, idx) => {
                if (idx != 0)
                  id += '-';
                id += elem;
              });
              const logo = data[2];
              const salario = data[3];
              this.datas.push(
                {
                  idx: idx,
                  name,
                  values,
                  position: this.cantToShow,
                  width: 0,
                  value: 0,
                  countryFlag,
                  id: id,
                  color: colors[data[1]],
                  flag: data[1],
                  logo,
                  year: data[1],
                  salario
                }
              );
            }
          });
        }
      }
    };
    rawFile.send(null);
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }  

  decimalValue(val) {
    return (val).toFixed(2);
  }

  floorValue(val) {
    return Math.round(val);
  }

  miles(cant) {
    cant = cant.trim();
    let solve = '';
    let cont = 1;
    for ( let i=cant.length - 1; i>=0; i -- ) {
      solve = cant[i] + solve;
      if ( cont % 3 === 0 && i > 0 ) {
        solve = '.' + solve;
      }
      cont ++;
    }
    return solve;
  }

  calculatePosition() {
    this.datas.forEach( (data, idx) => {
      let solve = 0;
      if ( data.value === 0 ) {
        data.position = this.cantToShow;
      } else {
        this.datas.forEach( (data1, idx1) => {
          if ( idx != idx1 ) {
            if ( data.width < data1.width )
              solve ++;
            else if ( data.width === data1.width && data.name < data1.name )
              solve ++;
          }
        });
        data.position = solve;
        if (data.position >= 12)
          data.position = this.cantToShow;
        if ( data.position < 10 )
          this.temp.add(data.name + '_' + data.idx)
      }      
    });
  }

  calculate(percent, position) {
    let max = 0;
    this.datas.forEach( data => {
      const diff = data.values[position + 1] - data.values[position];
      const ant = data.values[position];
      data.value = (ant + (percent * diff)/this.vel);
      max = Math.max(max, data.value);
    });
    this.datas.forEach( data => {
      data.width = isNaN( 30 * (data.value / max) ) ? 0 : 30 * (data.value / max);
    });
    this.calculatePosition();
  }

  async execute() {
    for ( let i=0; i<this.years.length; i++ ) {
      this.currentYear = this.years[i];
      let percent = 0;
      for ( let j=1; j <= this.vel; j ++ ) {
        await this.sleep(1);
        this.calculate(j, i);         
      }      
    }
    console.log(this.temp);
  }

  compute() {
    this.readTextFile(this.csv);
    this.execute();
  }

  async ngOnInit() {
    this.compute();
  }

  generarNumero(numero){
    return (Math.random()*numero).toFixed(0);
  }
  
  colorRGB(){
    var coolor = "("+this.generarNumero(255)+"," + this.generarNumero(255) + "," + this.generarNumero(255) +")";
    return "rgb" + coolor;
  }

}
