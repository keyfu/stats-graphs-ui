import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getValue'
})
export class GetValuePipe implements PipeTransform {

  transform(idx: any, actualYear: any, datas: any, max: any, ...args: any[]): any {
    return this.getValue(idx, actualYear, datas, max);
  }

  getValue(idx, actualYear, datas, max) {
    const t = Math.floor(actualYear);

    const diff = datas[idx].data[ t - 1890 + 1 ] - datas[idx].data[ t - 1890 ];
    const cant = datas[idx].data[ t - 1890 ];

    const percent = 100 * (actualYear - t);

    return Math.ceil(cant + (percent * diff) / 100);

    // return ( 90 * ((cant + ( (percent * diff) / 100 )) / this.max) ).toString() + '%';
  }

}
