import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getWidth'
})
export class GetWidthPipe implements PipeTransform {

  transform(idx: any, actualYear: any, datas: any, max: any, ...args: any[]): any {
    return this.getWidth(idx, actualYear, datas, max);
  }

  getWidth(idx, actualYear, datas, max) {
    const t = Math.floor(actualYear);

    const diff = datas[idx].data[ t - 1890 + 1 ] - datas[idx].data[ t - 1890 ];
    const cant = datas[idx].data[ t - 1890 ];

    const percent = 100 * (actualYear - t);

    return ( 80 * ((cant + ( (percent * diff) / 100 )) / max) ).toString() + '%';
  }

}
