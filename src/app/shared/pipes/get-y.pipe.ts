import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getY'
})
export class GetYPipe implements PipeTransform {

  transform(idx: any, actualYear: any, datas: any, max: any, ...args: any[]): any {
    return this.getY(idx, actualYear, datas, max);
  }

  getCurrentValue(id, actualYear, datas, max) {
    const t = Math.floor(actualYear);

    const diff = datas[id].data[t - 1890 + 1] - datas[id].data[t - 1890];
    const cant = datas[id].data[t - 1890];

    const percent = 100 * (actualYear - t);
    return (100 * ((cant + ((percent * diff) / 100)) / max));
  }

  getName(idx, datas) {
    return datas[idx].name;
  }

  getY(idx, actualYear, datas, max) {
    
    let solve = 0;
    const currentValue = this.getCurrentValue(idx, actualYear, datas, max);
    if (currentValue === 0)
      return 11 * 80;
    const currentName = this.getName(idx, datas);
    datas.forEach((value, index) => {
      const cv = this.getCurrentValue(index, actualYear, datas, max);
      if (currentValue < cv) {
        solve++;
      } else if (currentValue === cv && value.name < currentName) {
        solve++;
      }
    });
    return solve * 80;
  }

}
