import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphsTopCopyComponent } from './graphs-top-copy.component';

describe('GraphsTopCopyComponent', () => {
  let component: GraphsTopCopyComponent;
  let fixture: ComponentFixture<GraphsTopCopyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphsTopCopyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphsTopCopyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
