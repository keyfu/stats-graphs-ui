import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphsPercentComponent } from './graphs-percent.component';

describe('GraphsPercentComponent', () => {
  let component: GraphsPercentComponent;
  let fixture: ComponentFixture<GraphsPercentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphsPercentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphsPercentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
