import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graphs-titles',
  templateUrl: './graphs-titles.component.html',
  styleUrls: ['./graphs-titles.component.scss']
})
export class GraphsTitlesComponent implements OnInit {

  // Start Configuration
  max = 27;
  title = 'CAMPEONES EN SERIES MUNDIALES DE BEISBOL [1903-2019]';
  subtitle = '';
  initialYear = 1903;
  finalYear = 2019;
  total = 0;
  actualYear = 1903;
  actualYearCopy = 1903;
  cantToShow = 20;
  imagesFolder = 'assets/mlb/';
  csv = 'assets/csv/titles-mlb.csv';
  vel = 15;
  sum = 0.010;
  currentYear = 1889;
  // End Configuration

  datas = [];
  imageToEnlarge = [];
  imageToEnlargeCopy = [];
  position: string;
  cont = 0;
  contCopy = 0;
  years = [];

  constructor() { }

  truncate(text, cant) {
    return cant > text.length ? text.slice(0, cant) : text.slice(0, cant) + '...';
  }

  readTextFile(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          t.forEach((line, idx) => {
            if (idx === 0) {
              const data = line.split(',');
              this.years = data.slice(4).map(e => parseInt(e))
            }
            else if (line) {

              const data = line.split(',');
              const name = data[0].split(' ');
              const color = data[1]
              const style = { top: data[2].split('|')[0], left: data[2].split('|')[1] };
              const img = data[3];
              this.datas.push(
                {
                  id: name[0].toLowerCase(),
                  name: this.truncate(data[0], 18),
                  data: data.slice(4).map(e => parseInt(e)),
                  cantidadActual: 0,
                  width: '0%',
                  class: idx.toString(),
                  color,
                  img: this.imagesFolder + img + '.png',
                  position: this.cantToShow,
                  imagePosition: 0,
                  cantPosition: 0,
                  positions: [],
                  positionsTop: [],
                  imagePositions: [],
                  cantPositions: [],
                  cantidadActuals: [],
                  widths: [],
                  flag: 'assets/flags/' + data[2].toLowerCase() + '.png',
                  description: 'Este es un ejemplo de la description',
                  style
                }
              );
            }
          });
        }
      }
    };
    rawFile.send(null);
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  getValueByPosition(pos) {
    this.datas.forEach((data, idx) => {
      this.getValue(idx, pos);
    });
  }

  calculatePosition(idx) {
    let solve = 0;
    const currentValue = this.datas[idx].cantidadActuals[this.contCopy];
    if (currentValue === 0 || isNaN(currentValue)) {
      solve = this.cantToShow;
    } else {
      const currentName = this.getName(idx);
      this.datas.forEach((value, index) => {
        const cv = value.cantidadActuals[this.contCopy];
        if (currentValue < cv) {
          solve++;
        } else if (index != idx && currentValue === cv && currentName > this.getName(index)) {
          solve++;
        }
      });
    }
    return solve;
  }

  computePositions() {
    let solve = {};
    if (this.contCopy - 1 >= 0) {
      solve = { value: this.imageToEnlarge[this.contCopy - 1].value, style: this.imageToEnlarge[this.contCopy - 1].style };
    }
    this.datas.forEach((data, idx) => {
      const pos = this.calculatePosition(idx);
      data.positions[this.contCopy] = pos;
      data.positionsTop[this.contCopy] = pos > 0 ? this.cantToShow : pos;
    });
    this.datas.forEach((data, idx) => {
      if ( (this.contCopy - 1 >= 0 && data.cantidadActuals[this.contCopy - 1] != data.cantidadActuals[this.contCopy]) ) {
        solve = { value: data.img, style: data.style }
      }
    });
    return solve;
  }

  async getTime() {

    this.years.forEach((y, idx) => {
      const t = y - 1;
      if (idx < this.years.length - 1) {
        for (let i = t; i < t + 1; i += this.sum) {
          this.actualYearCopy = i;
          this.getValueByPosition(idx);
        }
      }
    });
    // calculate positions
    this.actualYearCopy = this.actualYear;
    this.years.forEach((y, idx) => {
      const a = y;
      if (idx < this.years.length - 1) {
        for (let i = a; i < a + 1; i += this.sum) {
          this.actualYearCopy = i;
          const t = this.computePositions();
          this.imageToEnlarge.push(t);
          this.contCopy++;
        }
      }
    });
    let contTmp = 0;
    for (let y of this.years) {
      if ( contTmp === 0 ) {
        contTmp ++;
        continue;
      }
      this.currentYear = y;
      const t = y;
      
        for (let i = t; i < t + 1; i += this.sum) {
          await this.sleep(this.vel);
          this.actualYear = i;
          if (this.cont < this.datas[0].positions.length - 1)
            this.cont++;
        }
      
      contTmp++;
    }

  }

  floorValue(val) {
    return Math.floor(val);
  }

  floorYear() {
    return Math.ceil(this.actualYear);
  }

  getName(idx) {
    return this.datas[idx].name;
  }

  getValue(idx, pos) {

    const t = Math.floor(this.actualYearCopy);

    const diff = this.datas[idx].data[pos + 1] - this.datas[idx].data[pos];
    const cant = this.datas[idx].data[pos];

    const percent = 100 * (this.actualYearCopy - t);

    const width = (80 * ((cant + ((percent * diff) / 100)) / this.max));

    this.datas[idx].cantidadActual = isNaN(Math.ceil(cant + (percent * diff) / 100)) ? 0 : Math.ceil(cant + (percent * diff) / 100);
    // this.datas[idx].cantidadActual = isNaN( cant + (percent * diff) / 100)  ? 0 : cant + (percent * diff) / 100;
    this.datas[idx].imagePosition = isNaN(width) ? '0%' : (width + 14).toString() + '%';
    this.datas[idx].cantPosition = isNaN(width) ? '0%' : (width + 17).toString() + '%';
    this.datas[idx].width = isNaN(width) ? '0%' : width.toString() + '%';

    this.datas[idx].imagePositions.push(this.datas[idx].imagePosition);
    this.datas[idx].cantPositions.push(this.datas[idx].cantPosition);
    this.datas[idx].cantidadActuals.push(this.datas[idx].cantidadActual);
    this.datas[idx].widths.push(this.datas[idx].width);
  }

  compute() {
    this.readTextFile(this.csv);
    this.getTime();
  }

  async ngOnInit() {
    this.compute();
  }

}
