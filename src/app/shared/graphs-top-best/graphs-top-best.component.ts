import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graphs-top-best',
  templateUrl: './graphs-top-best.component.html',
  styleUrls: ['./graphs-top-best.component.scss']
})
export class GraphsTopBestComponent implements OnInit {

  // Start Configuration
  title = 'PAíSES CON MAYOR ESPERANZA DE VIDA [2020]';
  subtitle = '';
  csv = 'assets/csv/esperanza-vida.csv';
  datas = [];
  datasToShow = [];
  paso = 0.10;
  cantToShow = 15;
  cont = 0;
  contCopy = 0;
  current = 0;
  top = 0;
  actual = 0;
  currentCopy = 0;
  // End Configuration

  max = 0;
  imageSelected = [];
  citySelected = [];

  cities = [];

  constructor() { }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  truncate(text, cant) {
    return cant > text.length ? text.slice(0, cant) : text.slice(0, cant) + '...';
  }

  hasKey(name) {
    let solve = -1;
    this.cities.forEach( (city, idx) => {
      if ( city.name === name )
        solve = idx;
    });
    return solve;
  }

  updatePosition() {
    this.cities.forEach( (city, idx) => {
      let pos = 0;
      this.cities.forEach( (city1, idx1) => {
        if ( idx1 !== idx ) {
          if ( city.cant < city1.cant )
            pos ++;
          else if ( city.cant === city1.cant && city.name > city1.name )
            pos ++;
        }
      });
      if ( city.cant === 0 )
        pos = 11
      city.position = pos;
    });
  }

  async compute() {
    this.readTextFile(this.csv);

    // ordenar de menor a mayor las cantidades
    this.datas.sort((a, b) => {
      if ( parseFloat(a.cant) > parseFloat(b.cant)) return 1;
      if ( parseFloat(a.cant) < parseFloat(b.cant)) return -1;
      return 0;
    });

    this.top = 51;

    this.datas.forEach( data => {
      if ( this.hasKey( data.countryReal ) === -1 ) {
        this.cities.push({
          name: data.countryReal,
          cant: 0,
          flag: data.country,
          position: 11
        });
      }
    });

    for ( let j=0; j < this.datas.length; j++ ) {
      await this.sleep(5000);
      const idx = this.hasKey( this.datas[j].countryReal );
      if ( this.hasKey( this.datas[j].countryReal ) !== -1 ) {
        this.cities[idx].cant ++;
        this.updatePosition();
      }
      this.top --;
      this.max = this.datas[j].cant;
      this.imageSelected = this.datas[j].imgCity;
      this.citySelected = this.datas[j].name;

      for ( let i=0; i < this.cont; i++ ) {
        this.datas[i].width = 1000 * ( this.datas[i].cant / this.max );
        this.datas[i].position ++;
      }

      this.datas[j].position = 0;
      this.datas[j].width = 1000;
      
      this.cont ++;
    }

  }

  getValue(top) {
    if ( top < 10 ) {
      return '0' + top.toString();
    }
    return top;
  }

  miles(cant) {
    // cant = cant.trim();
    cant = cant.toString();
    let solve = '';
    let cont = 1;
    for ( let i=cant.length - 1; i>=0; i -- ) {
      solve = cant[i] + solve;
      if ( cont % 3 === 0 && i > 0 ) {
        solve = '.' + solve;
      }
      cont ++;
    }
    return solve;
  }

  async ngOnInit() {
    this.compute();
  }

  readTextFile(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          t.forEach((line, idx) => {
            if (idx === 0) {
              const data = line.split(',');
            }
            else if (line) {
              const data = line.split(',');
              const name = data[0];
              const color = data[1];
              const country = data[2];
              const cant = data[3];
              console.log(name);
              
              this.datas.push({
                name,
                color,
                country,
                imgCity: 'assets/cities/' + name.toLowerCase() + '.jpg',
                cant: parseFloat(cant),
                width: 0,
                position: this.cantToShow,
                widthCircle1: 0,
                widthActuals: [],
                positionActuals: [],
                currentValues: [],
                cantPositions: [],
                countryPositions: []
              });
            }
          });
        }
      }
    };
    rawFile.send(null);
  }

}
