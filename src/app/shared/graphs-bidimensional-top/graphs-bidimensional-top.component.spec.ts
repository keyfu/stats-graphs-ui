import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphsBidimensionalTopComponent } from './graphs-bidimensional-top.component';

describe('GraphsBidimensionalTopComponent', () => {
  let component: GraphsBidimensionalTopComponent;
  let fixture: ComponentFixture<GraphsBidimensionalTopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphsBidimensionalTopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphsBidimensionalTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
