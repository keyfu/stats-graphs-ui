import { Component, OnInit } from '@angular/core';
import {colors} from '../const/colors';

@Component({
  selector: 'app-graphs-bidimensional-top',
  templateUrl: './graphs-bidimensional-top.component.html',
  styleUrls: ['./graphs-bidimensional-top.component.scss']
})
export class GraphsBidimensionalTopComponent implements OnInit {

  color_muerte = 'black';
  color_confirmados = 'rgb(83, 36, 132)';

  max_x = 10;
  max_y = 10;

  // Start Configuration
  title = 'Países más Libres del Mundo';
  subtitle = '';
  cantToShow = 710;
  currentYear = 0;
  timeSleep = 1000;
  flagcy = true;
  flagcx = true;
  // eje x
  // csv = 'assets/csv/total_cases_covid.csv';
  // eje y
  // csv1 = 'assets/csv/total_deaths_covid.csv';
  // csv = 'assets/csv/ICP.csv';
  // End Configuration
  csv = 'assets/csv/HF.csv'

  datas = [];
  datas1 = [];
  years = [];
  temp = new Set();
  currentFlag = '';
  init = 4;
  vel = 500;
  contttt = 0;

  constructor() { }

  readTextFile(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          t.forEach((line, idx) => {

            const data = line.split(',');
            if (idx !== 0 && data.length > 1 ) {
              const name = data[0];
              const personal_freedom = data[1];
              const economic_freedom = data[2];
              const human_freedom = data[3];
              const flag = data[4];
              const color = colors[ data[4] ];
              this.datas.unshift({
                name,
                personal_freedom,
                economic_freedom,
                human_freedom,
                flag,
                color,
                value_x: 0,
                value_y: 0,
                values_x: [],
                values_y: [],
                width: 0,
                widthD: 75,
                value: 0,
                death: 0,
              });
            }
              
          });
        }
      }
    };
    rawFile.send(null);
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  floorValue(val) {
    return Math.round(val);
  }

  decimalValue(val) {
    return (val).toFixed(2).toString().replace('.', ',');
  }

  calculate(position) {
    this.datas[position].values_x.push(parseFloat(this.datas[position].economic_freedom));
    this.datas[position].values_y.push(parseFloat(this.datas[position].personal_freedom));
    for(let i=0; i<position; i++) {
      this.datas[i].values_x.push(parseFloat(this.datas[i].values_x[position-1]));
      this.datas[i].values_y.push(parseFloat(this.datas[i].values_y[position-1]));
    }
    for(let i=position + 1; i<this.datas.length; i++) {
      this.datas[i].values_x.push(0);
      this.datas[i].values_y.push(0);
    }
  }

  run(percent, position) {
    let max = 10;
    let maxD = 10;
    this.datas.forEach(data => {
      const diff = data.values_x[position + 1] - data.values_x[position];
      const ant = data.values_x[position];

      const diffD = data.values_y[position + 1] - data.values_y[position];
      const antD = data.values_y[position];

      data.value = (ant + (percent * diff) / this.vel);
      data.death = (antD + (percent * diffD) / this.vel);

      max = Math.max(max, data.value);
      maxD = Math.max(maxD, data.death);
    });
    // this.max_x = max;
    // this.max_y = maxD;
    this.datas.forEach(data => {
      const t1 = isNaN( 80 * (data.value / max)) ? 0 : 80 * (data.value / max);
      data.width = t1;
      const t = isNaN( 75 * (data.death / maxD)) ? 0 : 75 * (data.death / maxD);
      data.widthD = 75 - t;
      
      if ( t1 === 80 && this.flagcx ) {
        setTimeout( () => {
          this.flagcx = false;
        }, 1000);       
      }

      if ( t === 75 && this.flagcy ) {
        setTimeout( () => {
          this.flagcy = false;
        }, 1000);
        // this.flagcy = false; 
        
      }
        
    });
  }

  async execute() {
    for(let i=0; i<this.datas.length; i++) {
      this.calculate(i);
    }
  }

  async compute() {
    this.readTextFile(this.csv);
    this.execute();
  }

  async ngOnInit() {
    this.compute();
  }

  async onCalculate(event) {
    this.currentYear = event;
    this.contttt ++;
    for (let j = 1; j <= this.vel; j++) {
      await this.sleep(1);
      this.run(j, 161 - event);
    }
  }

}
