import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphsRadialComponent } from './graphs-radial.component';

describe('GraphsRadialComponent', () => {
  let component: GraphsRadialComponent;
  let fixture: ComponentFixture<GraphsRadialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphsRadialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphsRadialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
