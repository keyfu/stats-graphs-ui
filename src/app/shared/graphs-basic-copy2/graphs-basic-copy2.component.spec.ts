import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphsBasicCopy2Component } from './graphs-basic-copy2.component';

describe('GraphsBasicCopy2Component', () => {
  let component: GraphsBasicCopy2Component;
  let fixture: ComponentFixture<GraphsBasicCopy2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphsBasicCopy2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphsBasicCopy2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
