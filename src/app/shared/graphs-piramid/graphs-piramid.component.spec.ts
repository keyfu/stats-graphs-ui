import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphsPiramidComponent } from './graphs-piramid.component';

describe('GraphsPiramidComponent', () => {
  let component: GraphsPiramidComponent;
  let fixture: ComponentFixture<GraphsPiramidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphsPiramidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphsPiramidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
