import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graphs-piramid',
  templateUrl: './graphs-piramid.component.html',
  styleUrls: ['./graphs-piramid.component.scss']
})
export class GraphsPiramidComponent implements OnInit {

  csv = 'assets/csv/population-cuba.csv';

  init = 1;
  vel = 1500;
  years = [];
  datas = [];
  currentYear = 1950;

  constructor() { }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  calculate(percent, position) {
    let max = 0;
    this.datas.forEach(data => {
      const diff = data.values[position + 1] - data.values[position];
      const ant = data.values[position];
      data.value = (ant + (percent * diff) / this.vel);
      max = Math.max(max, data.value);
    });
    this.datas.forEach(data => {
      data.width = isNaN( 100 * (data.value / max)) ? 0 : 100 * (data.value / max);
    });
    // this.calculatePosition();
  }

  async execute() {
    console.log(this.datas);
    for (let i = 0; i < this.years.length; i += 2) {
      // await this.sleep(1000);
      this.currentYear = this.years[i];
      console.log(this.currentYear);
      for (let j = 1; j <= this.vel; j++) {
        await this.sleep(1);
        this.calculate(j, i);
      }
    }
  }

  compute() {
    this.readTextFile(this.csv);
    this.execute();
  }

  async ngOnInit() {
    this.compute();
  }

  readTextFile(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          t.forEach((line, idx) => {

            const data = line.split(',');

            if (idx === 0) {
              this.years = data.slice(this.init);
            } else if (idx !== 0 && line) {
              
              let values = data.slice(this.init).map((element) => { 
                if ( isNaN(parseFloat(element.replace(/\s/g, ''))) ) {
                  return 0;
                }
                return parseFloat(element.replace(/\s/g, '')); 
              });
              values.unshift(0);
              this.datas.push(
                {
                  idx: idx,
                  values,
                  range: data[0],
                  width: 0,
                  value: 0
                }
              );
            }
          });
        }
      }
    };
    rawFile.send(null);
  }

}
