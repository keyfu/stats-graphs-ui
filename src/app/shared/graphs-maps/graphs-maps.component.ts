import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graphs-maps',
  templateUrl: './graphs-maps.component.html',
  styleUrls: ['./graphs-maps.component.scss']
})
export class GraphsMapsComponent implements OnInit {

  total = 6706002;
  vel = 200;
  dist = 10.6;

  points = [];
  // The smoothing ratio
  smoothing = 0.2

  // Start Configuration
  title = 'Turismo internacional en países Latinoamericanos';
  subtitle = 'Cantidad de turistas que arriban al país';
  cantToShow = 20;
  currentYear = 0;
  timeSleep = 1000;
  csv = 'assets/csv/myCOVID1.csv';
  csv1 = 'assets/csv/myCOVID_deaths1.csv';
  // End Configuration

  datas = [];
  years = [];
  totalCases = 0;
  totalDeaths = 0;

  path;
  tmp = 0;

  constructor() { }

  readTextFile1(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          t.forEach((line, idx) => {

            const data = line.split(',');

            if (idx === 0) {
              // this.years = data.slice(3);
            } else if (idx !== 0 && line) {

              this.datas[idx - 1].deaths = data.slice(5).map((element) => { return parseFloat(element); });
              this.datas[idx - 1].deaths.unshift(0);
              // console.log(this.datas[idx-1], idx);
            }
          });
        }
      }
    };
    rawFile.send(null);
  }

  readTextFile(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          t.forEach((line, idx) => {

            const data = line.split(',');

            if (idx === 0) {
              this.years = data.slice(5);
            } else if (idx !== 0 && line) {
              const name = data[0];
              let values = data.slice(5).map((element) => { return parseFloat(element); });
              values.unshift(0);
              const countryFlag = data[3];
              let id = '';
              name.split(' ').forEach((elem, idx) => {
                if (idx != 0)
                  id += '-';
                id += elem;
              });
              const points = data.slice(5).map( (elem, idx1) => {
                return [
                  idx1 * this.dist,
                  -1*( parseFloat(elem) )
                ]
              });
              const pointsFalse = data.slice(5).map( (elem, idx1) => {
                  return [
                    idx1 * this.dist,
                    -1*( (parseFloat(elem) / 3000) )
                  ];
              }); 
              points.unshift([0, 0]);
              pointsFalse.unshift([0, 0]);
              this.datas.push(
                {
                  points,
                  pointsFalse,
                  name,
                  values,
                  position: this.cantToShow,
                  width: 0,
                  value: 0,
                  countryFlag,
                  id: id,
                  color: data[4],
                  flag: data[1],
                  top: data[1],
                  left: data[2],
                  deaths: [],
                  positionXValue: -100,
                  positionYValue: 0,
                }
              );
            }
          });
        }
      }
    };
    rawFile.send(null);
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  floorValue(val) {
    return Math.round(val);
  }

  miles(cant) {
    cant = cant.trim();
    let solve = '';
    let cont = 1;
    for (let i = cant.length - 1; i >= 0; i--) {
      solve = cant[i] + solve;
      if (cont % 3 === 0 && i > 0) {
        solve = '.' + solve;
      }
      cont++;
    }
    return solve;
  }

  calculatePosition() {
    this.datas.forEach((data, idx) => {
      let solve = 0;
      if (data.width === 0) {
        data.position = this.cantToShow;
      } else {
        this.datas.forEach((data1, idx1) => {
          if (idx != idx1) {
            if (data.width < data1.width)
              solve++;
            else if (data.width === data1.width && data.name < data1.name)
              solve++;
          }
        });
        data.position = solve;
      }
    });
  }

  calculate(percent, position) {
    let max = 0;
    this.datas.forEach(data => {
      const diff = data.values[position + 1] - data.values[position];
      const diff_death = data.deaths[position + 1] - data.deaths[position];
      /*
      if (diff < 0) {
        console.log(diff, this.currentYear);
      }
      if (diff_death < 0) {
        console.log(diff_death, this.currentYear);
      }
      */
      const ant = data.values[position];
      const ant_death = data.deaths[position];
      data.value = (ant + (percent * diff) / this.vel);
      data.death = (ant_death + (percent * diff_death) / this.vel);
      max = Math.max(max, data.value);
    });
    let totalCases = 0;
    let totalDeaths = 0;
    this.datas.forEach((data, idx) => {
      if (idx != this.datas.length - 1) {
        totalCases += data.value;
        totalDeaths += data.death;
        data.width = isNaN(75 * (data.value / max)) ? 0 : 75 * (data.value / max);
      }
    });
    this.totalCases = totalCases;
    this.totalDeaths = totalDeaths;
    this.calculatePosition();
  }

  async execute() {
    await this.sleep(1000);
    this.datas.forEach(data => {
      this.points = data.pointsFalse;
      const tmp1 = this.svgPath(this.points, this.bezierCommand);
      this.path = document.getElementById(data.id);
      this.path.setAttribute('d', tmp1);
    });
    for (let i = 0; i < this.years.length; i++) {
      this.currentYear = this.years[i];
      let percent = 0;
      for (let j = 1; j <= this.vel; j++) {
        await this.sleep(1);
        this.tmp += (1/this.vel)*(this.dist);
        this.calculate(j, i);
      }
    }
  }

  compute() {
    this.readTextFile(this.csv);
    this.readTextFile1(this.csv1);
    this.execute();
  }

  async ngOnInit() {
    this.compute();
  }

  generarNumero(numero) {
    return (Math.random() * numero).toFixed(0);
  }

  colorRGB() {
    var coolor = "(" + this.generarNumero(255) + "," + this.generarNumero(255) + "," + this.generarNumero(255) + ")";
    return "rgb" + coolor;
  }

  translateYear(currentYear) {
    const d = currentYear.split(' ');
    let solve = d[1] + '-' + d[0].split('.')[0] + '-' + d[2];
    return solve;
  }

  svgPath = (points, command) => {
    // build the d attributes by looping over the points
    const d = points.reduce((acc, point, i, a) => i === 0
      ? `M ${point[0]},${point[1]}`
      : `${acc} ${command(point, i, a)}`
      , '')
    return d
  }

  bezierCommand = (point, i, a) => {

    // start control point
    const cps = this.controlPoint(a[i - 1], a[i - 2], point, false);

    // end control point
    const cpe = this.controlPoint(point, a[i - 1], a[i + 1], true)
    return `C ${cps[0]},${cps[1]} ${cpe[0]},${cpe[1]} ${point[0]},${point[1]}`
  }

  controlPoint = (current, previous, next, reverse) => {

    const p = previous || current
    const n = next || current

    // Properties of the opposed-line
    const o = this.line(p, n)

    // If is end-control-point, add PI to the angle to go backward
    const angle = o.angle + (reverse ? Math.PI : 0)
    const length = o.length * this.smoothing

    // The control point position is relative to the current point
    const x = current[0] + Math.cos(angle) * length
    const y = current[1] + Math.sin(angle) * length
    return [x, y]
  }

  line = (pointA, pointB) => {
    const lengthX = pointB[0] - pointA[0]
    const lengthY = pointB[1] - pointA[1]
    return {
      length: Math.sqrt(Math.pow(lengthX, 2) + Math.pow(lengthY, 2)),
      angle: Math.atan2(lengthY, lengthX)
    }
  }

}
