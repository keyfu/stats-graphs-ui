import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graphs-top-new',
  templateUrl: './graphs-top-new.component.html',
  styleUrls: ['./graphs-top-new.component.scss']
})
export class GraphsTopNewComponent implements OnInit {

  // Start Configuration
  title = 'Futbolistas con más goles en un mismo club';
  // subtitle = 'Cantidad de goles anotados con el club';
  csv = 'assets/csv/goleadores-clubes.csv';
  datas = [];
  datasToShow = [];
  paso = 0.10;
  cantToShow = 16;
  cont = 0;
  contCopy = 0;
  current = 0;
  top = 0;
  actual = 0;
  currentCopy = 0;
  
  vel1 = 4;
  vel = this.vel1 * 100;
  // End Configuration

  max = 0;
  imageSelected = '';
  citySelected = '';

  imageSelected1 = '';
  citySelected1 = '';

  icons = [
    {
      img: 'balon-futbol.png',
      positionX: 200,
      positionY: 0
    },
    {
      img: 'balon-basket.png',
      positionX: 1300,
      positionY: -3
    },
    {
      img: 'balon-nfl.png',
      positionX: 800,
      positionY: -7
    },
    {
      img: 'formula1.png',
      positionX: 500,
      positionY: -10
    },
    {
      img: 'tennis.png',
      positionX: 200,
      positionY: -15
    },
    {
      img: 'cricket.png',
      positionX: 500,
      positionY: -18
    },
    {
      img: 'boxeo.png',
      positionX: 900,
      positionY: -22
    },
    {
      img: 'balon-golf.png',
      positionX: 600,
      positionY: -27
    },
    {
      img: 'balon-baseball.png',
      positionX: 200,
      positionY: -30
    }
  ];

  constructor() { }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  truncate(text, cant) {
    return cant > text.length ? text.slice(0, cant) : text.slice(0, cant) + '...';
  }

  floorValue(val) {
    return Math.round(val);
  }

  async compute() {
    this.readTextFile(this.csv);

    // ordenar de menor a mayor las cantidades
    this.datas.sort((a, b) => {
      if ( parseFloat(a.cant) > parseFloat(b.cant)) return 1;
      if ( parseFloat(a.cant) < parseFloat(b.cant)) return -1;
      if ( a.id < b.id )
        return 1
      return -1
    });

    this.top = this.datas.length + 1;

    // start precalc
    for (let j = 0; j < this.datas.length; j++) {
      this.max = this.datas[j].cant;
      // await this.sleep(4000);
      for (let i = 0; i < this.cont; i++) {
        const width = ((this.datas[i].cant / this.max) * 70);
        const diff = this.datas[i].widthInt - width;
        let t = j * this.vel + 1;
        for (let k = this.vel; k > 0; k--) {
          const widthActual = (width + ( ((k/this.vel)) * diff ) );
          this.datas[i].widthActuals[t] = 1*(widthActual);
          this.datas[i].currentValues[t] = this.datas[i].cant;
          this.datas[i].countryPositions[t] = ( widthActual + 10 ).toString() + '%';
          this.datas[i].cantPositions[t] = ( widthActual + 19 ).toString() + '%';
          t++;
          this.datas[i].widthInt = width;
          this.datas[i].width = width.toString() + '%';
          // this.datas[i].positionY = 8*(widthActual);
        }
      }
      this.currentCopy = j * this.vel;
      for (let i = 1; i <= this.vel; i ++ ) {
        const value = (i/this.vel) * parseFloat(this.datas[j].cant);
        this.datas[j].width = i.toString() + '%';
        this.currentCopy++;
        this.datas[j].widthActuals[this.currentCopy] = 1*i;
        this.datas[j].currentValues[this.currentCopy] = parseFloat( (value).toString() ).toFixed(1);
        this.datas[j].widthInt = i;
        this.datas[j].countryPositions[this.currentCopy] = (i + 10).toString() + '%';
        this.datas[j].cantPositions[this.currentCopy] = (i + 19).toString() + '%';
        // this.datas[j].positionY = 8*i;
      }
      this.cont++;
    }
    // end precalc

    this.cont = 0;

    await this.sleep(1000);
    for (let j = 0; j < this.datas.length; j++) {
      await this.sleep(20000);
      this.top --;
      this.max = this.datas[j].cant;
      if ( j%2 !== 0 ) {
        this.imageSelected1 = this.datas[j].imgCity;
        this.citySelected1 = this.datas[j].name;
      }
      else {
        this.imageSelected = this.datas[j].imgCity;
        this.citySelected = this.datas[j].name;  
      }
      

      for (let i = 0; i < this.cont; i++) {
          this.datas[i].position ++;
          this.datas[i].positionY = (100 - 100*(this.datas[i].cant / this.max));
          if ( this.datas[j].mult != 0 ) {
            this.datas[i].positionY *= 60 * ( (this.datas[i].mult - this.datas[j].mult) + 1 );
          } else {
            this.datas[i].positionY *= 30 * ( (this.datas[i].mult - this.datas[j].mult) + 1 );
          }
      }

      this.datas[j].position = 0;
      this.datas[j].positionY = 0;
      // this.current = j * 700;
      for (let i = 0; i < this.vel; i ++) {
          this.icons.forEach( icon => {
            icon.positionY += 0.001;
          })
          await this.sleep(this.vel1).then( () => {
            this.current++;
          }
        );
      }
      this.cont++;
    }

  }

  getValue(top) {
    if ( top < 10 ) {
      return '0' + top.toString();
    }
    return top;
  }

  miles(cant) {
    cant = cant.trim();
    let solve = '';
    let cont = 1;
    for ( let i=cant.length - 1; i>=0; i -- ) {
      solve = cant[i] + solve;
      if ( cont % 3 === 0 && i > 0 ) {
        solve = '.' + solve;
      }
      cont ++;
    }
    return solve;
  }

  async ngOnInit() {
    this.compute();
  }

  readTextFile(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          t.forEach((line, idx) => {
            if (idx === 0) {
              const data = line.split(',');
            }
            else if (line) {
              const data = line.split(',');
              const name = data[0];
              const color = this.colorHEX();
              const country = data[2].toLowerCase();
              const cant = data[3];
              const nameShort = data[2]
              this.datas.push({
                id: idx,
                name,
                color,
                country,
                imgCity: 'assets/universidades-mexico/' + name + '.jpg',
                cant,
                width: '0%',
                position: this.cantToShow,
                widthActuals: [],
                positionActuals: [],
                currentValues: [],
                cantPositions: [],
                countryPositions: [],
                // positionX: this.getPositionRandom(),
                positionX: data[5],
                nameShort,
                positionY: 1000,
                flag: data[4],
                sport: data[6],
                mult: data[7]
              });
            }
          });
        }
      }
    };
    rawFile.send(null);
  }

  generarLetra(){
    var letras = ["a","b","c","d","e","f","0","1","2","3","4","5","6","7","8","9"];
    var numero = (Math.random()*15).toFixed(0);
    return letras[numero];
  }
    
  colorHEX(){
    var coolor = "";
    for(var i=0;i<6;i++){
      coolor = coolor + this.generarLetra() ;
    }
    return "#" + coolor;
  }

  getPositionRandom(){
    return Math.floor(Math.random() * 700) + 500;
  }

}
