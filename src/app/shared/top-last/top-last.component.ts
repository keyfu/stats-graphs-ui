import { Component, OnInit } from '@angular/core';
import { colors } from '../const/colors';

@Component({
  selector: 'app-top-last',
  templateUrl: './top-last.component.html',
  styleUrls: ['./top-last.component.scss']
})
export class TopLastComponent implements OnInit {

  colors = colors;

  // Start Configuration
  title = 'DESIGUALDAD DE GÉNEROS [2021]';
  subtitle = 'Índice global de brecha de género (1=equidad; 0=inequidad)';
  csv = 'assets/csv/desigualdad-de-generos.csv';
  datas = [];
  datasToShow = [];
  paso = 0.10;
  cantToShow = 17;
  cont = 0;
  contCopy = 0;
  current = 0;
  top = 0;
  actual = 0;
  currentCopy = 0;
  topCont = 0;
  // End Configuration

  max = 0;
  imageSelected = '';
  citySelected = '';

  imageSelected1 = '';
  citySelected1 = '';

  rowSelected = {
    class: '',
    name: '',
    value: '',
    classValue: '',
    img: '',
    classImg: '',
    inst: '',
    classInst: '',
    country: '',
    number: 51
  };

  constructor() { }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  truncate(text, cant) {
    return cant > text.length ? text.slice(0, cant) : text.slice(0, cant) + '...';
  }

  async compute() {
    this.readTextFile(this.csv);

    // ordenar de menor a mayor las cantidades
    this.datas.sort((a, b) => {
      if (parseFloat(a.cant) < parseFloat(b.cant)) return - 1;
      if (parseFloat(a.cant) > parseFloat(b.cant)) return 1;
      if (parseFloat(a.cant) == parseFloat(b.cant)) {
        if (a.number > b.number)
          return -1;
        return 1;
      }
      return 0;
    });
    this.topCont = this.datas.length;
    for ( let i=0; i<this.datas.length; i++ ) {
      this.rowSelected.class = 'animate__backInLeft';
      this.rowSelected.classValue = 'animate__backInRight';
      this.rowSelected.classImg = 'animate__bounceIn';
      this.rowSelected.classInst = 'animate__bounceInUp';
      this.rowSelected.name = this.datas[i].name;
      this.rowSelected.img = this.datas[i].img;
      this.rowSelected.inst = this.datas[i].inst;
      this.rowSelected.country = this.datas[i].country;
      this.rowSelected.number = this.datas[i].number;
      const div = this.datas[i].cant / 250;
      for (let j=0; j<this.datas[i].cant; j+= div) {
        await this.sleep(1);
        this.rowSelected.value = parseFloat(j.toString()).toFixed(1);  
      }
      this.rowSelected.value = this.datas[i].cant;
      await this.sleep(6000);
      this.rowSelected.class = 'animate__backOutRight';
      this.rowSelected.classValue = 'animate__backOutLeft';
      this.rowSelected.classImg = 'animate__bounceOut';
      this.rowSelected.classInst = 'animate__bounceOutDown';
      await this.sleep(1000);
      this.rowSelected.img = '';
      this.rowSelected.country = '';
      this.rowSelected.value = '';
      await this.sleep(1000);
      this.topCont --;
    }

  }


  async ngOnInit() {
    this.compute();
  }

  readTextFile(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          let number = 1;
          t.forEach((line, idx) => {
            if (idx === 0) {
              const data = line.split(',');
            }
            else if (line) {
              const data = line.split(',');
              const name = data[0].trim();
              const color = data[1];
              const country = data[3];
              let country1, country2;
              const inst = data[5];
              /*
              if (data[4].split('|').length >= 2) {
                country1 = data[4].split('|')[1];
              }
              if (data[4].split('|').length >= 3) {
                country2 = data[4].split('|')[2];
              }
              */
              const cant = parseFloat(data[3]);
              const nameShort = data[2].trim();
              this.datas.push({
                number,
                name,
                color,
                flag: data[4],
                country: data[2],
                country1,
                country2,
                imgCity: 'assets/cities/' + data[5] + '.jpg',
                cant,
                width: '0%',
                position: this.cantToShow,
                widthActuals: [],
                positionActuals: [],
                currentValues: [],
                cantPositions: [],
                countryPositions: [],
                nameShort,
                img: country,
                pib: data[6],
                inst
              });
              number++;
            }
          });
        }
      }
    };
    rawFile.send(null);
  }

  miles(cant) {
    cant = cant.toString();
    let solve = '';
    let cont = 1;
    for ( let i=cant.length - 1; i>=0; i -- ) {
      solve = cant[i] + solve;
      if ( cont % 3 === 0 && i > 0 ) {
        solve = '.' + solve;
      }
      cont ++;
    }
    return solve;
  }

}
