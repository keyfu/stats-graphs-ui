import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-graphs-curv',
  templateUrl: './graphs-curv.component.html',
  styleUrls: ['./graphs-curv.component.scss']
})
export class GraphsCurvComponent implements OnInit {

  // The smoothing ratio
  smoothing = 0.2

  points = [];

  percents = [ 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 ];
  percentsn = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];

  dates = [];

  path;
  circle
  fch = [];
  datas = [];
  tmp = 100;
  positionRect = -100;
  positionValue = -100;
  positionYValue = 0;
  cant = "0";
  currentDate = 0;

  constructor() { }

  async ngOnInit() {
    // this.circle = document.getElementById('circle');
    // this.circle.setAttribute('path', tmp);

    this.readTextFile('./assets/csv/Acciones.csv');
    await this.sleep(1000);
    this.datas.forEach( data => {
      this.points = data.pointsFalse;
      const tmp = this.svgPath(this.points, this.bezierCommand);
      this.path = document.getElementById(data.id);
      this.path.setAttribute('d', tmp);
    });

    for( let i=1; i<=9; i++ ) {
      this.currentDate = i - 1;
      for ( let j=1; j<=100; j++ ) {
        await this.sleep(10);
        this.positionRect += 1;
        this.datas.forEach( element => {
          const diff = element.points[ i ][1] - element.points[ i - 1 ][1];
          const diffFalse = element.pointsFalse[ i ][1] - element.pointsFalse[ i - 1 ][1];
          if ( diff < 0 ) {
            element.cant = ( element.points[ i - 1 ][1] - ( (j/100) * -1*diff )).toFixed(2);
            element.cantFalse = ( element.pointsFalse[ i - 1 ][1] - ( (j/100) * -1*diffFalse )).toFixed(2);
          } else {
            element.cant = (element.points[ i - 1 ][1] + ( (j/100) * diff )).toFixed(2);
            element.cantFalse = (element.pointsFalse[ i - 1 ][1] + ( (j/100) * diffFalse )).toFixed(2);
          }
          element.positionXValue += 1;
          element.positionYValue = parseFloat(element.cantFalse);
        });

        this.datas.forEach( element => {
          let position = 0;
          this.datas.forEach( element1 => {
            if ( parseFloat(element.cant) > parseFloat(element1.cant) ) {
              position ++;
            } else if ( parseFloat(element1.cant) == parseFloat(element.cant) && element1.name < element.name ) {
              position ++;
            }
          });
          element.position = position;
        })
      }
    }

    for ( let i=10; i<=this.fch.length; i++ ) {
      this.currentDate = i - 1;
      for ( let j=1; j<=100; j++ ) {
        await this.sleep(10);
        this.fch.forEach( elem => {
          elem.position -= 0.010;
        });
        this.tmp -= 1;
        this.datas.forEach( element => {
          const diff = element.points[ i ][1] - element.points[ i - 1 ][1];
          const diffFalse = element.pointsFalse[ i ][1] - element.pointsFalse[ i - 1 ][1];
          if ( diff < 0 ) {
            element.cant = ( element.points[ i - 1 ][1] - ( (j/100) * -1*diff )) .toFixed(2);
            element.cantFalse = ( element.pointsFalse[ i - 1 ][1] - ( (j/100) * -1*diffFalse )) .toFixed(2);
          } else {
            element.cant = (element.points[ i - 1 ][1] + ( (j/100) * diff )).toFixed(2);
            element.cantFalse = (element.pointsFalse[ i - 1 ][1] + ( (j/100) * diffFalse )).toFixed(2);
          }
          element.positionXValue += 1;
          element.positionYValue = parseFloat(element.cantFalse);
        });

        this.datas.forEach( element => {
          let position = 0;
          this.datas.forEach( element1 => {
            if ( parseFloat(element.cant) > parseFloat(element1.cant) ) {
              position ++;
            } else if ( parseFloat(element1.cant) == parseFloat(element.cant) && element1.name < element.name ) {
              position ++;
            }
          });
          element.position = position;
        })
      }
    }

  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  line = (pointA, pointB) => {
    const lengthX = pointB[0] - pointA[0]
    const lengthY = pointB[1] - pointA[1]
    return {
      length: Math.sqrt(Math.pow(lengthX, 2) + Math.pow(lengthY, 2)),
      angle: Math.atan2(lengthY, lengthX)
    }
  }

  controlPoint = (current, previous, next, reverse) => {

    const p = previous || current
    const n = next || current

    // Properties of the opposed-line
    const o = this.line(p, n)

    // If is end-control-point, add PI to the angle to go backward
    const angle = o.angle + (reverse ? Math.PI : 0)
    const length = o.length * this.smoothing

    // The control point position is relative to the current point
    const x = current[0] + Math.cos(angle) * length
    const y = current[1] + Math.sin(angle) * length
    return [x, y]
  }

  bezierCommand = (point, i, a) => {

    // start control point
    const cps = this.controlPoint(a[i - 1], a[i - 2], point, false);

    // end control point
    const cpe = this.controlPoint(point, a[i - 1], a[i + 1], true)
    return `C ${cps[0]},${cps[1]} ${cpe[0]},${cpe[1]} ${point[0]},${point[1]}`
  }

  svgPath = (points, command) => {
    // build the d attributes by looping over the points
    const d = points.reduce((acc, point, i, a) => i === 0
      ? `M ${point[0]},${point[1]}`
      : `${acc} ${command(point, i, a)}`
      , '')
    return d
  }

  readTextFile(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          t.forEach((line, idx) => {
            if ( idx === 0 ) {
              this.fch = line.split(',').slice(3).map( (element, idx) => {
                return {
                  value: element,
                  position: idx + 1
                }
              });

            } else if (idx !== 0 && line) {
              
              const data = line.split(',');
              const name = data[0].split(' ');
              const color = data[1];
              const img = data[3];
              const points = data.slice(3).map( (elem, idx1) => {
                return [
                  idx1 * 3,
                  -1*( parseFloat(elem) )
                ]
              });
              const pointsFalse = data.slice(3).map( (elem, idx1) => {
                return [
                  idx1 * 3,
                  -1*( parseFloat(elem) * 0.8 )
                ]
              }); 
              points.unshift([0, 0]);
              pointsFalse.unshift([0, 0]);
              this.datas.push(
                {
                  id: name[0].toLowerCase(),
                  name: data[0],
                  points,
                  pointsFalse,
                  positionXValue: -100,
                  positionYValue: 0,
                  cant: 0,
                  flag: data[2],
                  color: data[1],
                  position: idx - 1
                }
              );
            }
          });
        }
      }
    };
    rawFile.send(null);
  }

  miles(cant) {

    cant = cant.toString().trim();
    let solve = '';
    let cont = 1;
    for ( let i=cant.length - 1; i>=0; i -- ) {
      solve = cant[i] + solve;
      if ( cont % 3 === 0 && i > 0 ) {
        solve = '.' + solve;
      }
      cont ++;
    }
    return solve;
  }

}
