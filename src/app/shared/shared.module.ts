import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BarComponent } from './bar/bar.component';
import { GetYPipe } from './pipes/get-y.pipe';
import { GetWidthPipe } from './pipes/get-width.pipe';
import { GetXImagePipe } from './pipes/get-ximage.pipe';
import { GetXPipe } from './pipes/get-x.pipe';
import { GetValuePipe } from './pipes/get-value.pipe';
import { GraphsBasicComponent } from './graphs-basic/graphs-basic.component';
import { GraphsTitlesComponent } from './graphs-titles/graphs-titles.component';
import { GraphsTopComponent } from './graphs-top/graphs-top.component';
import { GraphsTopBestComponent } from './graphs-top-best/graphs-top-best.component';
import { GraphsVsComponent } from './graphs-vs/graphs-vs.component';
import { GraphsBasicTmpComponent } from './graphs-basic-tmp/graphs-basic-tmp.component';
import { GraphsCurvComponent } from './graphs-curv/graphs-curv.component';
import { GraphsBasicCopyComponent } from './graphs-basic-copy/graphs-basic-copy.component';
import { GraphsBasicCopy1Component } from './graphs-basic-copy1/graphs-basic-copy1.component';
import { GraphsMapsComponent } from './graphs-maps/graphs-maps.component';
import { GraphsTopNewComponent } from './graphs-top-new/graphs-top-new.component';
import { GraphsBasicBendComponent } from './graphs-basic-bend/graphs-basic-bend.component';
import { GraphsBasicNewComponent } from './graphs-basic-new/graphs-basic-new.component';
import { GraphsBidimensionalComponent } from './graphs-bidimensional/graphs-bidimensional.component';
import { GraphsRadialComponent } from './graphs-radial/graphs-radial.component';
import { GraphsVsTrofeoComponent } from './graphs-vs-trofeo/graphs-vs-trofeo.component';
import { GraphsPercentComponent } from './graphs-percent/graphs-percent.component';
import { GaugeModule } from 'angular-gauge';
import { GraphsBasicTenComponent } from './graphs-basic-ten/graphs-basic-ten.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { TopLastComponent } from './top-last/top-last.component';
import { GraphsVsCountryComponent } from './graphs-vs-country/graphs-vs-country.component';
import { MatCardModule, MatGridListModule } from '@angular/material';
import { GraphsBasicCopy2Component } from './graphs-basic-copy2/graphs-basic-copy2.component';
import { GraphsPiramidComponent } from './graphs-piramid/graphs-piramid.component';
import { GraphsBidimensionalTopComponent } from './graphs-bidimensional-top/graphs-bidimensional-top.component';
import { GraphsTopCopyComponent } from './graphs-top-copy/graphs-top-copy.component';



@NgModule({
  declarations: [BarComponent, GetYPipe, GetWidthPipe, GetXImagePipe, GetXPipe, GetValuePipe, GraphsBasicComponent, GraphsTitlesComponent, GraphsTopComponent, GraphsTopBestComponent, GraphsVsComponent, GraphsBasicTmpComponent, GraphsCurvComponent, GraphsBasicCopyComponent, GraphsMapsComponent, GraphsTopNewComponent, GraphsBasicBendComponent, GraphsBasicNewComponent, GraphsBidimensionalComponent, GraphsRadialComponent, GraphsVsTrofeoComponent, GraphsPercentComponent, GraphsBasicCopy1Component, GraphsBasicTenComponent, TopLastComponent, GraphsVsCountryComponent, GraphsBasicCopy2Component, GraphsPiramidComponent, GraphsBidimensionalTopComponent, GraphsTopCopyComponent],
  imports: [
    CommonModule,
    GaugeModule.forRoot(),
    AngularFontAwesomeModule,
    MatCardModule,
    MatGridListModule
    
  ],
  exports: [
    BarComponent,
    GetYPipe,
    GetWidthPipe,
    GetXImagePipe,
    GetXPipe,
    GetValuePipe,
    GraphsBasicComponent,
    GraphsTitlesComponent,
    GraphsTopComponent,
    GraphsTopBestComponent,
    GraphsVsComponent,
    GraphsBasicTmpComponent,
    GraphsCurvComponent,
    GraphsBasicCopyComponent,
    GraphsMapsComponent,
    GraphsTopNewComponent,
    GraphsBasicBendComponent,
    GraphsBasicNewComponent,
    GraphsBidimensionalComponent,
    GraphsRadialComponent,
    GraphsVsTrofeoComponent,
    GraphsPercentComponent,
    GraphsBasicCopy1Component,
    GraphsBasicTenComponent,
    TopLastComponent,
    GraphsVsCountryComponent,
    GraphsPiramidComponent,
    GraphsBidimensionalTopComponent
  ]
})
export class SharedModule { }
