import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graphs-basic-bend',
  templateUrl: './graphs-basic-bend.component.html',
  styleUrls: ['./graphs-basic-bend.component.scss']
})
export class GraphsBasicBendComponent implements OnInit {

  // Start Configuration
  // title = 'Países con mayor obesidad';
  // subtitle = 'Porcentaje de obesidad en adultos';
  cantToShow = 20;
  currentYear = 0;
  timeSleep = 1000;
  csv = 'assets/csv/obesidad mundial.csv';
  // End Configuration

  datas = [];
  years = [];
  temp = new Set();

  constructor() { }

  readTextFile(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          t.forEach((line, idx) => {
            
            const data = line.split(',');

            if ( idx === 0 ) {
              this.years = data.slice(3);
            } else if (idx !== 0 && line) {
              const name = data[0];
              let values = data.slice(3).map( (element) => { return parseFloat(element); } );
              values.unshift(0);
              const countryFlag = data[2];
              let id = '';
              name.split(' ').forEach( (elem, idx) => {
                if ( idx != 0 )
                  id += '-';
                id += elem;
              });
              this.datas.push(
                {
                  name,
                  values,
                  position: this.cantToShow,
                  width: 0,
                  value: 0,
                  countryFlag,
                  id: id,
                  color: data[2],
                  flag: data[1]
                }
              );
            }
          });
        }
      }
    };
    rawFile.send(null);
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }  

  floorValue(val) {
    return Math.round(val);
  }

  decimalValue(val) {
    return (val).toFixed(2);
  }

  miles(cant) {
    cant = cant.trim();
    let solve = '';
    let cont = 1;
    for ( let i=cant.length - 1; i>=0; i -- ) {
      solve = cant[i] + solve;
      if ( cont % 3 === 0 && i > 0 ) {
        solve = '.' + solve;
      }
      cont ++;
    }
    return solve;
  }

  calculatePosition() {
    this.datas.forEach( (data, idx) => {
      let solve = 0;
      if ( data.width === 0 ) {
        data.position = this.cantToShow;
      } else {
        this.datas.forEach( (data1, idx1) => {
          if ( idx != idx1 ) {
            if ( data.width < data1.width )
              solve ++;
            else if ( data.width === data1.width && data.name < data1.name )
              solve ++;
          }
        });
        data.position = solve;
      }
      if ( data.position <= 5 ) {
        this.temp.add(data.name);
      }     
    });
  }

  calculate(percent, position) {
    let max = 0;
    this.datas.forEach( data => {
      const diff = data.values[position + 1] - data.values[position];
      const ant = data.values[position];
      data.value = (ant + (percent * diff)/1500);
      max = Math.max(max, data.value);
    });
    this.datas.forEach( data => {
      data.width = isNaN( 60 * (data.value / max) ) ? 0 : 60 * (data.value / max);
    });
    this.calculatePosition();
  }

  async execute() {
    for ( let i=0; i<this.years.length; i++ ) {
      this.currentYear = this.years[i];
      let percent = 0;
      for ( let j=1; j <= 1500; j ++ ) {
        await this.sleep(1);
        this.calculate(j, i);         
      }      
    }
    console.log(this.temp);
  }

  compute() {
    this.readTextFile(this.csv);
    this.execute();
  }

  async ngOnInit() {
    this.compute();
  }

  generarNumero(numero){
    return (Math.random()*numero).toFixed(0);
  }
  
  colorRGB(){
    var coolor = "("+this.generarNumero(255)+"," + this.generarNumero(255) + "," + this.generarNumero(255) +")";
    return "rgb" + coolor;
  }

}
