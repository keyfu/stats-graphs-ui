import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphsBasicBendComponent } from './graphs-basic-bend.component';

describe('GraphsBasicBendComponent', () => {
  let component: GraphsBasicBendComponent;
  let fixture: ComponentFixture<GraphsBasicBendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphsBasicBendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphsBasicBendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
