import { Component, OnInit } from '@angular/core';

enum COLOR {
  LOST = '#c80f2e',
  WIN = '#1d418b',
  TIE = '#EDAD08'
}

const LGVALUE = {
  t: 1,
  jj: 1,
  min: 1,
  pts: 1,
  tca: 1,
  tci: 1,
  'tca%': 1,
  '3pt': 1,
  '3pti': 1,
  '3pt%': 1,
  tl: 1,
  tli: 1,
  'tl%': 1,
  reb: 1,
  ast: 1,
  bp: -1,
  br: 1,
  tp: 1,
  fc: -1,
  dd: 1,
  td: 1
}


@Component({
  selector: 'app-graphs-vs',
  templateUrl: './graphs-vs.component.html',
  styleUrls: ['./graphs-vs.component.scss']
})
export class GraphsVsComponent implements OnInit {
  // legend basket
  
  legend = [
    {
      id: 't',
      text: 'Temporadas'
    },
    {
      id: 'jj',
      text: 'Juegos Jugados'
    },
    {
      id: 'min',
      text: 'Minutos Jugados'
    },
    {
      id: 'pts',
      text: 'Puntos Anotados'
    },
    {
      id: 'tca',
      text: 'Tiros de Campo Anotados'
    },
    {
      id: 'tci',
      text: 'Tiros de Campo Intentados'
    },
    {
      id: 'tca%',
      text: 'Porcentaje Tiros de Campo Anotados'
    },
    {
      id: '3pt',
      text: 'Triples Anotados'
    },
    {
      id: '3pti',
      text: 'Triples Intentados'
    },
    {
      id: '3pt%',
      text: 'Porcentaje de Triples Anotados'
    },
    {
      id: 'tl',
      text: 'Tiros Libres Anotados'
    },
    {
      id: 'tli',
      text: 'Tiros Libres Intentados'
    },
    {
      id: 'tl%',
      text: 'Porcentaje Tiros Libres Anotados'
    },
    {
      id: 'reb',
      text: 'Rebotes'
    },
    {
      id: 'ast',
      text: 'Asistencias'
    },
    {
      id: 'bp',
      text: 'Balones Perdidos'
    },
    {
      id: 'br',
      text: 'Balones Robados'
    },
    {
      id: 'tp',
      text: 'Tapones'
    },
    {
      id: 'fc',
      text: 'Faltas Cometidas'
    },
    {
      id: 'dd',
      text: 'Doble Doble'
    },
    {
      id: 'td',
      text: 'Triple Doble'
    },
  ];

  // legend futbol
  /*
  legend = [
    {
      id: 't',
      text: 'Temporadas'
    },
    {
      id: 'jj',
      text: 'Juegos Jugados'
    },
    {
      id: 'mj',
      text: 'Minutos Jugados'
    },
    {
      id: 'g',
      text: 'Goles'
    },
    {
      id: 'a',
      text: 'Asistencias'
    },
    {
      id: 'p',
      text: 'Penaltis'
    },
    {
      id: 'tl',
      text: 'Tiros Libres'
    },
    {
      id: 'ht+',
      text: 'Hack-trick o más'
    },
    {
      id: 'ta',
      text: 'Tarjetas Amarillas'
    },
    {
      id: 'mxg',
      text: 'Minutos por Gol'
    },
    {
      id: 'gxj',
      text: 'Goles por juego'
    }
  ];

  */
  // legend titles
  /*
  legend = [
    {
      id: 'bo',
      text: 'Balón de Oro'
    },
    {
      id: 'mje',
      text: 'Mejor Jugador en Europa'
    },
    {
      id: 'mft',
      text: 'Mejor Futbolista de la Temporada'
    },
    {
      id: 'lc',
      text: 'Liga de Campeones'
    },
    {
      id: 'cln',
      text: 'Campeon Liga Nacional'
    },
    {
      id: 'se',
      text: 'Supercopa de Europa'
    },
    {
      id: 's',
      text: 'Supercopas'
    },
    {
      id: 'o',
      text: 'Olimpiada'
    },
    {
      id: 'cs',
      text: 'Copas con la Selección'
    }
  ];
  */

  datas = [];
  csv = 'assets/csv/Curry-vs-Iverson/per game post season.csv';
  nameColumn = [];
  title = 'UNO VS UNO';
  subtitle = 'Promedios en postemporada';
  currentStats = 0;
  total = 800;

  constructor() { }

  async ngOnInit() {

    await this.sleep(2000);
    this.readTextFile(this.csv);
    for (let i = 0; i < this.nameColumn.length; i++) {
      await this.sleep(5000);
      this.currentStats = i;
      const max = Math.max(this.datas[0].stats[i], this.datas[1].stats[i]);
      let color1 = COLOR.TIE;
      let color2 = COLOR.TIE;
      if (this.datas[0].stats[i] > this.datas[1].stats[i]) {
        color1 = COLOR.WIN;
        color2 = COLOR.LOST;
      } else if (this.datas[0].stats[i] < this.datas[1].stats[i]) {
        color1 = COLOR.LOST;
        color2 = COLOR.WIN;
      }

      this.datas[0].currentStats[i] =
      {
        value: max == 0 ? this.total - 100 : (this.total - 100) * (this.datas[0].stats[i] / max),
        color: color1
      }
      this.datas[1].currentStats[i] =
      {
        value: max == 0 ? this.total - 100 : (this.total - 100) * (this.datas[1].stats[i] / max),
        color: color2
      }
    }
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  readTextFile(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          t.forEach((line, idx) => {
            if (idx === 0) {
              const data = line.split(',');
              this.nameColumn = data.slice(1).map(element => { return element.toLowerCase().trim(); });
            }
            else if (line) {
              const data = line.split(',');
              const name = data[0];
              this.datas.push({
                name,
                stats: data.slice(1).map(element => { return parseFloat(element); }),
                currentStats: data.slice(1).map((e) => { return { value: 0, color: 'red' } })
              });
            }
          });
        }
      }
    };
    rawFile.send(null);
  }

  getColor(player, idx, nameStat: any) {

    const player2 = player === 0 ? 1 : 0;
    const mult = parseInt(LGVALUE[nameStat.toLowerCase()]);

    if (this.datas[player].stats[idx] * mult > this.datas[player2].stats[idx] * mult)
      return COLOR.WIN;
    else if (this.datas[player].stats[idx] * mult < this.datas[player2].stats[idx] * mult)
      return COLOR.LOST;
    return COLOR.TIE;
  }

}
