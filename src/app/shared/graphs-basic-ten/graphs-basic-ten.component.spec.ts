import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphsBasicTenComponent } from './graphs-basic-ten.component';

describe('GraphsBasicTenComponent', () => {
  let component: GraphsBasicTenComponent;
  let fixture: ComponentFixture<GraphsBasicTenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphsBasicTenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphsBasicTenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
