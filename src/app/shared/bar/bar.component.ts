import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

let test = [];
for ( let i = 0; i <= 150; i++ ) {
  test.push(
    state( (i + 1).toString(), style({
      top: (i * 80).toString() + 'px'
    }))
  )
}

test.push(
  transition('* => *', [
    animate('1s')
  ])
);

@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.scss'],
  animations: [
    trigger('photoState', test)
  ]
})
export class BarComponent implements OnInit {

  @Input() time = 1000;
  @Input() img;
  @Input() color = 'red';
  @Input() width = '0%';
  @Input() id = '';
  @Input() data = [];
  @Input() max;
  @Input() backgroundColor = 'br';
  @Input() class = 'a';
  @Input() set execute(val) {
    this._execute = val;
    if (this._execute) {
      this.index++;
      if (this.index < this.data.length) {
        this.exec();
      }
    }
  }
  @Output() finished = new EventEmitter<boolean>();
  @Output() cantidadActual = new EventEmitter<any>();
  @Output() setYear = new EventEmitter<any>();

  _execute = true;
  @Input() cant = 0;
  year = 1890;
  index = 0;

  constructor() { }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  ngOnInit() {
    this.setYear.emit(this.year);
  }

  async exec() {

    const diff = this.data[ this.index ] - this.data[ this.index - 1 ];
    const plus = diff / this.time;
    let sum = this.data[ this.index - 1 ];
    for ( let i = 0; i < this.time; i++ ) {
      await this.sleep(1);
      sum += plus;
      this.width = (82 * (sum / this.max)).toFixed(2).toString() + '%';
      /*
      this.cantidadActual.emit({
        id: this.id,
        value: sum
      });
      */
    }
    this.finished.emit(true);
    this.setYear.emit(this.year);
  }



}
