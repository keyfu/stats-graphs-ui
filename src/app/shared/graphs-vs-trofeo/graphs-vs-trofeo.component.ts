import { Component, OnInit } from '@angular/core';

enum COLOR {
  LOST = '#c80f2e',
  WIN = '#1d418b',
  TIE = '#EDAD08'
}

const LGVALUE = {
  'all-star game': 1,
  'season mvp': 1,
  'all-nba first team': 1,
  'all-nba teams': 1,
  'scoring leader': 1,
  'steal leader': 1,
  'rookie of the year': 1,
}


@Component({
  selector: 'app-graphs-vs-trofeo',
  templateUrl: './graphs-vs-trofeo.component.html',
  styleUrls: ['./graphs-vs-trofeo.component.scss']
})
export class GraphsVsTrofeoComponent implements OnInit {
  // legend basket
  
  legend = [
    {
      id: 'all-star game',
      text: 'Juego de las Estrellas'
    },
    {
      id: 'season mvp',
      text: 'Jugador más valioso de la Temporada'
    },
    {
      id: 'all-nba first team',
      text: 'Mejor Quinteto de la NBA'
    },
    {
      id: 'all-nba teams',
      text: 'Mejores Quintetos de la NBA'
    },
    {
      id: 'scoring leader',
      text: 'Líder en Puntos'
    },
    {
      id: 'steal leader',
      text: 'Líder en Robos'
    },
    {
      id: 'rookie of the year',
      text: 'Novato del Año'
    }
  ];

  // legend futbol
  /*
  legend = [
    {
      id: 't',
      text: 'Temporadas'
    },
    {
      id: 'jj',
      text: 'Juegos Jugados'
    },
    {
      id: 'mj',
      text: 'Minutos Jugados'
    },
    {
      id: 'g',
      text: 'Goles'
    },
    {
      id: 'a',
      text: 'Asistencias'
    },
    {
      id: 'p',
      text: 'Penaltis'
    },
    {
      id: 'tl',
      text: 'Tiros Libres'
    },
    {
      id: 'ht+',
      text: 'Hack-trick o más'
    },
    {
      id: 'ta',
      text: 'Tarjetas Amarillas'
    },
    {
      id: 'mxg',
      text: 'Minutos por Gol'
    },
    {
      id: 'gxj',
      text: 'Goles por juego'
    }
  ];

  */
  // legend titles
  /*
  legend = [
    {
      id: 'bo',
      text: 'Balón de Oro'
    },
    {
      id: 'mje',
      text: 'Mejor Jugador en Europa'
    },
    {
      id: 'mft',
      text: 'Mejor Futbolista de la Temporada'
    },
    {
      id: 'lc',
      text: 'Liga de Campeones'
    },
    {
      id: 'cln',
      text: 'Campeon Liga Nacional'
    },
    {
      id: 'se',
      text: 'Supercopa de Europa'
    },
    {
      id: 's',
      text: 'Supercopas'
    },
    {
      id: 'o',
      text: 'Olimpiada'
    },
    {
      id: 'cs',
      text: 'Copas con la Selección'
    }
  ];
  */

  datas = [];
  csv = 'assets/csv/Curry-vs-Iverson/premios.csv';
  nameColumn = [];
  title = 'UNO VS UNO';
  subtitle = 'Honores y Premios';
  currentStats = 0;
  total = 800;

  constructor() { }

  async ngOnInit() {

    await this.sleep(2000);
    this.readTextFile(this.csv);
    for (let i = 0; i < this.nameColumn.length; i++) {
      await this.sleep(5000);
      this.currentStats = i;
      const max = Math.max(this.datas[0].stats[i], this.datas[1].stats[i]);
      let color1 = COLOR.TIE;
      let color2 = COLOR.TIE;
      if (this.datas[0].stats[i] > this.datas[1].stats[i]) {
        color1 = COLOR.WIN;
        color2 = COLOR.LOST;
      } else if (this.datas[0].stats[i] < this.datas[1].stats[i]) {
        color1 = COLOR.LOST;
        color2 = COLOR.WIN;
      }

      this.datas[0].currentStats[i] =
      {
        value: max == 0 ? this.total - 100 : (this.total - 100) * (this.datas[0].stats[i] / max),
        color: color1
      }
      this.datas[1].currentStats[i] =
      {
        value: max == 0 ? this.total - 100 : (this.total - 100) * (this.datas[1].stats[i] / max),
        color: color2
      }
    }
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  readTextFile(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          t.forEach((line, idx) => {
            if (idx === 0) {
              const data = line.split(',');
              this.nameColumn = data.slice(1).map(element => { return element.toLowerCase().trim(); });
            }
            else if (line) {
              const data = line.split(',');
              const name = data[0];
              this.datas.push({
                name,
                stats: data.slice(1).map(element => { return parseFloat(element); }),
                currentStats: data.slice(1).map((e) => { return { value: 0, color: 'red' } })
              });
            }
          });
        }
      }
    };
    rawFile.send(null);
  }

  getColor(player, idx, nameStat: any) {

    const player2 = player === 0 ? 1 : 0;
    const mult = parseInt(LGVALUE[nameStat.toLowerCase()]);

    if (this.datas[player].stats[idx] * mult > this.datas[player2].stats[idx] * mult)
      return COLOR.WIN;
    else if (this.datas[player].stats[idx] * mult < this.datas[player2].stats[idx] * mult)
      return COLOR.LOST;
    return COLOR.TIE;
  }

}
