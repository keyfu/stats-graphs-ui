import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graphs-basic-tmp',
  templateUrl: './graphs-basic-tmp.component.html',
  styleUrls: ['./graphs-basic-tmp.component.scss']
})
export class GraphsBasicTmpComponent implements OnInit {

  // Start Configuration
  max = 762;
  title = 'TOTAL DE INFECTADOS (CASOS) POR PAÍS EN EL MUNDO';
  subtitle = 'Actualizado 30 de Abril de 2020';
  initialYear = 0;
  finalYear = 2020;
  total = 0;
  actualYear = 0;
  actualYearCopy = 0;
  cantToShow = 20;
  imagesFolder = 'assets/nba/';
  csv = 'assets/csv/jonroneros.csv';
  vel = 20;
  sum = 0.010;
  currentDate = 0;
  // End Configuration

  datas = [];
  imageToEnlarge = [];
  imageToEnlargeCopy = [];
  position: string;
  cont = 0;
  contCopy = 0;
  fch = [];
  world = [];

  constructor() { }

  truncate(text, cant) {
    return cant > text.length ? text.slice(0, cant) : text.slice(0, cant) + '...';
  }

  readTextFile(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          t.forEach((line, idx) => {
            if ( idx === 0 ) {
              this.fch = line.split(',').slice(1);
            } else if (idx !== 0 && line) {
              
              const data = line.split(',');
              const name = data[0].split(' ');
              const color = data[1];
              const img = data[3];
              this.datas.push(
                {
                  id: name[0].toLowerCase(),
                  name: this.truncate(data[0], 12),
                  data: data.slice(1).map(e => parseInt(e)),
                  cantidadActual: 0,
                  width: '0%',
                  class: idx.toString(),
                  color,
                  img,
                  position: this.cantToShow,
                  imagePosition: 0,
                  cantPosition: 0,
                  positions: [],
                  positionsTop: [],
                  imagePositions: [],
                  cantPositions: [],
                  cantidadActuals: [],
                  widths: [],
                  flag: 'assets/flags/' + data[2].toLowerCase() + '.png',
                  description: 'Este es un ejemplo de la description'
                }
              );
            }
          });
        }
      }
    };
    rawFile.send(null);
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  getValueByPosition(percent) {
    let suma = 0;
    this.datas.forEach((data, idx) => {
      suma += this.getValue(idx, percent);
    });

    return suma;
  }

  calculatePosition(idx) {
    let solve = 0;
    const currentValue = this.datas[idx].cantidadActuals[this.contCopy];
    if (currentValue === 0 || isNaN(currentValue)) {
      solve = this.cantToShow;
    } else {
      const currentName = this.getName(idx);
      this.datas.forEach((value, index) => {
        const cv = value.cantidadActuals[this.contCopy];
        if (currentValue < cv) {
          solve++;
        } else if ( index != idx && currentValue === cv &&  this.datas[idx].cantidadActuals[this.contCopy - 1] > value.cantidadActuals[this.contCopy - 1]) {
          solve++;
        } else if ( index != idx && currentValue === cv &&  this.datas[idx].cantidadActuals[this.contCopy - 1] === value.cantidadActuals[this.contCopy - 1] && currentName > this.getName(index) )
          solve ++;
      });
    }
    return solve;
  }

  computePositions() {
    let maxValue = 0;
    let position = 0;
    this.datas.forEach( (data, idx) => {
      const pos = this.calculatePosition(idx);
      data.positions[this.contCopy] = pos;
      data.positionsTop[this.contCopy] = pos > 0 ? 10 : pos;
      if ( data.cantidadActuals[this.contCopy] > maxValue ) {
        position = idx;
        maxValue = data.cantidadActuals[this.contCopy];
      }
    });
    this.imageToEnlarge[this.contCopy] = {
      description: this.datas[position].description,
      value: this.datas[position].img,
      opacity: 1
    }
    
  }

  async getTime() {
    for (let i = 0; i < this.fch.length - 1; i ++) {
      for ( let j=1; j <= 100; j++ ) {
        this.actualYearCopy = i;
        this.world.push(this.getValueByPosition(j));
      }
    }
    // calculate positions
    this.actualYearCopy = this.actualYear;
    for (let i = 0; i < this.fch.length - 1; i += this.sum) {
      this.actualYearCopy = i;
      this.computePositions();
      this.contCopy ++;
    }
    for (let i = 0; i < this.fch.length - 1; i += this.sum) {
      await this.sleep(this.vel);
      this.actualYear = i;
      if ( this.cont < this.datas[0].positions.length - 1 )
        this.cont ++;
    }

  }

  floorValue(val) {
    return Math.floor(val);
  }

  floorYear() {
    return Math.ceil(this.actualYear);
  }

  getName(idx) {
    return this.datas[idx].name;
  }

  getValue(idx, percent) {
    const t = this.actualYearCopy

    const diff = this.datas[idx].data[t - this.initialYear + 1] - this.datas[idx].data[t - this.initialYear];
    const cant = this.datas[idx].data[t - this.initialYear];    

    // const percent = 100 * (this.actualYearCopy - t);

    const width = (80 * ((cant + ((percent * diff) / 100)) / this.max));

    this.datas[idx].cantidadActual = isNaN( Math.round(cant + (percent * diff) / 100) ) ? 0 : Math.round(cant + (percent * diff) / 100);
    this.datas[idx].imagePosition = isNaN( width ) ? '0%' : (width + 9).toString() + '%';
    this.datas[idx].cantPosition = isNaN( width ) ? '0%' : (width + 11).toString() + '%';
    this.datas[idx].width = isNaN( width ) ? '0%' : width.toString() + '%';

    this.datas[idx].imagePositions.push( this.datas[idx].imagePosition);
    this.datas[idx].cantPositions.push(this.datas[idx].cantPosition);
    this.datas[idx].cantidadActuals.push( this.floorValue(this.datas[idx].cantidadActual) );
    this.datas[idx].widths.push(this.datas[idx].width);

    return this.datas[idx].cantidadActual;
  }

  compute() {
    this.readTextFile(this.csv);
    this.getTime();
  }

  async ngOnInit() {
    this.compute();
  }

}
