import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graphs-vs-country',
  templateUrl: './graphs-vs-country.component.html',
  styleUrls: ['./graphs-vs-country.component.scss']
})
export class GraphsVsCountryComponent implements OnInit {

  datas = [
    {
      title: 'Esperanza de Vida (en años)',
      csv: 'assets/csv/argentina-chile-brasil-mexico-esperanza.csv',
      fixed: 2,
      fuente: 'BANCO MUNDIAL'
    },
    {
      title: 'Mortalidad Infantil (por cada 1000 nacidos)',
      csv: 'assets/csv/argentina-chile-brasil-mexico-mortalidad-infantil.csv',
      fixed: 2,
      fuente: 'BANCO MUNDIAL',
      inverse: true
    },
    {
      title: 'Índice de Desarrollo Humano',
      csv: 'assets/csv/argentina-chile-brasil-mexico-idh.csv',
      fixed: 3,
      fuente: 'PROGRAMA DE LAS NACIONES UNIDAS PARA EL DESARROLLO'
    },
    {
      title: 'PIB per capita (en dólares)',
      csv: 'assets/csv/argentina-chile-brasil-mexico-gdp-per-capita.csv',
      fixed: 2,
      fuente: 'BANCO MUNDIAL'
    },
    {
      title: 'PIB (en billones de dólares)',
      csv: 'assets/csv/argentina-chile-brasil-mexico-gdp.csv',
      fixed: 3,
      fuente: 'FONDO MONETARIO INTERNACIONAL'
    },
    {
      title: 'Población urbana',
      csv: 'assets/csv/argentina-chile-brasil-mexico-poblacion.csv',
      fixed: 0,
      fuente: 'BANCO MUNDIAL'
    },
    {
      title: 'Exportaciones (en millones de dólares)',
      csv: 'assets/csv/argentina-chile-brasil-mexico-exportaciones.csv',
      fixed: 0,
      fuente: 'BANCO MUNDIAL'
    },
    {
      title: 'Importaciones (en millones de dólares)',
      csv: 'assets/csv/argentina-chile-brasil-mexico-importaciones.csv',
      fixed: 0,
      fuente: 'BANCO MUNDIAL'
    },
    {
      title: 'Presupuesto Militar (en millones de dólares)',
      csv: 'assets/csv/argentina-chile-brasil-mexico-presupuesto.csv',
      fixed: 1,
      fuente: 'BANCO MUNDIAL'
    }
  ];

  currentYear = '';

  constructor() { }

  ngOnInit() {
  }

  onChangeYear(year) {
    this.currentYear = year;
  }

}
