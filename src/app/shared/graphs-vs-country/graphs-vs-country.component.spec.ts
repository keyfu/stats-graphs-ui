import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphsVsCountryComponent } from './graphs-vs-country.component';

describe('GraphsVsCountryComponent', () => {
  let component: GraphsVsCountryComponent;
  let fixture: ComponentFixture<GraphsVsCountryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphsVsCountryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphsVsCountryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
