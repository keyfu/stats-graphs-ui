import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graphs-bidimensional',
  templateUrl: './graphs-bidimensional.component.html',
  styleUrls: ['./graphs-bidimensional.component.scss']
})
export class GraphsBidimensionalComponent implements OnInit {

  color_muerte = 'black';
  color_confirmados = 'rgb(83, 36, 132)';

  max_x = 0;
  max_y = 0;

  // Start Configuration
  title = '... 9 meses de Pandemia ...';
  subtitle = '';
  cantToShow = 710;
  currentYear = 0;
  timeSleep = 1000;
  flagcy = true;
  flagcx = true;
  // eje x
  csv = 'assets/csv/total_cases_covid.csv';
  // eje y
  csv1 = 'assets/csv/total_deaths_covid.csv';
  // csv = 'assets/csv/ICP.csv';
  // End Configuration

  datas = [];
  datas1 = [];
  years = [];
  temp = new Set();
  currentFlag = '';
  init = 4;
  vel = 50;

  constructor() { }

  readTextFile1(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          t.forEach((line, idx) => {

            const data = line.split(',');

            if (idx === 0) {
              // this.years = data.slice(3);
            } else if (idx !== 0 && line) {
              if ( this.datas[idx-1] ) {
                const pob = parseFloat(data[3]);
                let deaths = data.slice(this.init).map((element) => { 
                  return 1000000*(parseFloat(element)/pob);  
                });
                deaths.unshift(0);
                this.datas[idx-1].deaths = deaths;
              }
            }
          });
          
        }
      }
    };
    rawFile.send(null);
  }

  readTextFile(file) {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const t = rawFile.responseText.split('\n');
          t.forEach((line, idx) => {

            const data = line.split(',');

            if (idx === 0) {
              this.years = data.slice(this.init);
            } else if (idx !== 0 && line) {
              const name = data[0];
              const pob = parseFloat(data[3]);
              let values = data.slice(this.init).map((element) => { 
                if ( isNaN(parseFloat(element)) ) {
                  return 0;
                }
                return 1000000*(parseFloat(element)/pob); 
                // return parseFloat(element); 
              });
              values.unshift(0);
              const countryFlag = data[2];
              let id = '';
              name.split(' ').forEach((elem, idx) => {
                if (idx != 0)
                  id += '-';
                id += elem;
              });
              const logo = data[2];
              
              this.datas.push(
                {
                  idx: idx,
                  name,
                  values,
                  position: this.cantToShow,
                  width: 0,
                  widthD: 75,
                  value: 0,
                  death: 0,
                  countryFlag,
                  id: id,
                  color: data[2],
                  flag: data[1],
                  logo,
                  year: data[1],
                  deaths: [],
                }
              );
            }
          });
        }
      }
    };
    rawFile.send(null);
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  floorValue(val) {
    return Math.round(val);
  }

  decimalValue(val) {
    return (val).toFixed(2).toString().replace('.', ',');
  }

  miles(cant) {
    cant = cant.trim();
    let solve = '';
    let cont = 1;
    for (let i = cant.length - 1; i >= 0; i--) {
      solve = cant[i] + solve;
      if (cont % 3 === 0 && i > 0) {
        solve = '.' + solve;
      }
      cont++;
    }
    return solve;
  }

  calculatePosition() {
    this.datas.forEach((data, idx) => {
      let solve = 0;      
      if (data.width === 0) {
        data.position = this.cantToShow;
      } else {
        this.datas.forEach((data1, idx1) => {
          if (idx != idx1) {
            if (data.width < data1.width)
              solve++;
            else if (data.width === data1.width && data.name < data1.name)
              solve++;
          }
        });
        data.position = solve;
      }
    });
  }

  calculate(percent, position) {
    let max = 0;
    let maxD = 0;
    this.datas.forEach(data => {
      const diff = data.values[position + 1] - data.values[position];
      const ant = data.values[position];

      const diffD = data.deaths[position + 1] - data.deaths[position];
      const antD = data.deaths[position];

      data.value = (ant + (percent * diff) / this.vel);
      data.death = (antD + (percent * diffD) / this.vel);

      max = Math.max(max, data.value);
      maxD = Math.max(maxD, data.death);
    });
    this.max_x = max;
    this.max_y = maxD;
    this.datas.forEach(data => {
      const t1 = isNaN( 80 * (data.value / max)) ? 0 : 80 * (data.value / max);
      data.width = t1;
      const t = isNaN( 75 * (data.death / maxD)) ? 0 : 75 * (data.death / maxD);
      data.widthD = 75 - t;
      
      if ( t1 === 80 && this.flagcx ) {
        setTimeout( () => {
          this.flagcx = false;
        }, 1000);       
      }

      if ( t === 75 && this.flagcy ) {
        setTimeout( () => {
          this.flagcy = false;
        }, 1000);
        // this.flagcy = false; 
        
      }
        
    });

    this.calculatePosition();
  }

  async execute() {
    for (let i = 0; i < this.years.length; i++) {
      this.currentYear = this.years[i];
      for (let j = 1; j <= this.vel; j++) {
        await this.sleep(1);
        this.calculate(j, i);
      }
    }
    console.log(this.temp);
  }

  async compute() {
    this.readTextFile(this.csv);
    this.readTextFile1(this.csv1);
    this.execute();
  }

  async ngOnInit() {
    this.compute();
  }

  generarNumero(numero) {
    return (Math.random() * numero).toFixed(0);
  }

  colorRGB() {
    var coolor = "(" + this.generarNumero(255) + "," + this.generarNumero(255) + "," + this.generarNumero(255) + ")";
    return "rgb" + coolor;
  }

}
