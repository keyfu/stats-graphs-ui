

import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  /*
  datas = [
    {
      from: '0%',
      to: '50%'
    },
    {
      from: '0%',
      to: '30%'
    }
  ];

  time = 0;

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  */

  async ngOnInit() {
    /*
    for ( let i=0; i<1; i++ ){
      await this.sleep(10000);
      this.datas[0].from = this.datas[0].to;
      this.datas[0].to = '70%';
      this.datas[1].from = this.datas[1].to;
      this.datas[1].to = '80%';
    }
    */
  }

}